var app = angular.module('flapperNews', []);


//factory to handle dynamic posts
app.factory('posts', [function(){

    //o = object instance of an array called posts.
    //return o
    var o = {
        posts: []
    };
    return o;
}]);




//main Controller
app.controller('MainCtrl', [
'$scope',
'posts',
function($scope, posts){

//***possibly incorrect***/
angular.module('flapperNews', ['ui.router'])

    //manual array of title posts
  // $scope.posts = [
  //     {title: 'post 1', upvotes: 5},
  //     {title: 'post 2', upvotes: 2},
  //     {title: 'post 3', upvotes: 15},
  //     {title: 'post 4', upvotes: 9},
  //     {title: 'post 5', upvotes: 4}
  // ];


//bind post services to $scope
$scope.posts = posts.posts;


//get input from form sent with angular model
  $scope.addPost = function(){
      if(!$scope.title || $scope.title === '')  //check for null input submits
        {return;}
      $scope.posts.push({
          title: $scope.title,
          link: $scope.link,
          upvotes: 0
      });
      $scope.link ='';
      $scope.title = '';
  };

  //increment post upvotes
  $scope.incrementUpvotes = function(post) {
      post.upvotes += 1;
  };
}]);
